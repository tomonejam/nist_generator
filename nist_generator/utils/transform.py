import numpy as np
import cv2 as cv

from random import uniform, randint


class BiasedNoise(object):
    def __init__(self, ratio, kernel_size):
        self.ratio = ratio
        self.kernel_size = kernel_size

    def __call__(self, image):

        # Determine noise ratio
        ratio = self.ratio
        if isinstance(ratio, list) or isinstance(ratio, tuple):
            ratio = uniform(min(ratio), max(ratio))

        # Determine kernel size
        kernel_size = self.kernel_size
        if isinstance(self.kernel_size, list) or isinstance(self.kernel_size, tuple):
            kernel_size = randint(min(kernel_size), max(kernel_size))

        # Noising
        pixel_max = np.max(image)
        pixel_min = np.min(image)
        pixel_average = np.mean(image)

        mask = ratio > (
            1.0
            - (
                np.random.uniform(size=image.shape)
                * (image - pixel_min)
                / (pixel_max - pixel_min)
            )
        )
        mask = mask.astype(image.dtype)

        elem = cv.getStructuringElement(cv.MORPH_ELLIPSE, (kernel_size, kernel_size))
        mask = cv.dilate(mask, elem)

        image[mask > 0] = pixel_average

        return image


class RandomErode(object):
    def __init__(self, ratio, kernel_size, fg_thresh, fg_erode_kernel_size):
        self.ratio = ratio
        self.kernel_size = kernel_size
        self.fg_thresh = fg_thresh
        self.fg_erode_kernel_size = fg_erode_kernel_size

    def __call__(self, image):

        # Pre-eroding for input image
        elem = cv.getStructuringElement(
            cv.MORPH_ELLIPSE, (self.fg_erode_kernel_size, self.fg_erode_kernel_size)
        )
        erode_image = cv.erode(image, elem)

        # Determine noise ratio
        ratio = self.ratio
        if isinstance(ratio, list) or isinstance(ratio, tuple):
            ratio = uniform(min(ratio), max(ratio))

        # Determine kernel size
        kernel_size = self.kernel_size
        if isinstance(self.kernel_size, list) or isinstance(self.kernel_size, tuple):
            kernel_size = randint(min(kernel_size), max(kernel_size))

        # Determine foreground threshold
        fg_thresh = self.fg_thresh
        if isinstance(fg_thresh, float):
            fg_thresh = int(np.max(image) * fg_thresh)

        # DESTROY THAT IMAGE!!!!! HAHAHAHAHAHAHAHAHA ΣΣΣ (」○ ω○ )／
        fg_mask = erode_image > self.fg_thresh
        fg_mask_view = fg_mask.reshape(-1)

        fg_mask_ind = np.arange(fg_mask_view.shape[-1])[fg_mask_view]
        np.random.shuffle(fg_mask_ind)
        fg_mask_ind = fg_mask_ind[: int(ratio * len(fg_mask_ind))]

        mask = np.zeros(fg_mask.shape, dtype=image.dtype)
        mask_view = mask.reshape(-1)
        mask_view[fg_mask_ind] = 1

        elem = cv.getStructuringElement(cv.MORPH_ELLIPSE, (kernel_size, kernel_size))
        mask = cv.dilate(mask, elem)

        image[mask > 0] = np.min(image)

        return image
