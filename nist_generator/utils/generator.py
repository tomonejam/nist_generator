import numpy as np
import cv2 as cv

from os import listdir
from os.path import exists, isdir, expanduser, join, basename

from copy import deepcopy
from glob import glob
from random import randrange, randint, random
from tqdm import tqdm

from . import RandomGet


class RandomStringGenerator(object):
    def __init__(
        self,
        char_set,
        char_min_len,
        char_max_len,
        space_max_strike,
        space_min_len,
        space_max_len,
    ):

        # Parse character set
        if isinstance(char_set, list):
            self.char_set = char_set
        elif exists(char_set):
            with open(char_set, "r") as f:
                self.char_set = [s.strip("\r\n") for s in f.readlines()]

        # Assign values
        self.char_min_len = char_min_len
        self.char_max_len = char_max_len
        self.space_max_strike = space_max_strike
        self.space_min_len = space_min_len
        self.space_max_len = space_max_len

    def __call__(self):

        string = [
            self.char_set[randrange(0, len(self.char_set))]
            for _ in range(randint(self.char_min_len, self.char_max_len))
        ]

        space_strike = sorted(
            [
                randrange(1, len(string))
                for _ in range(randint(0, self.space_max_strike))
            ],
            reverse=True,
        )

        for idx in space_strike:
            string.insert(idx, " " * randint(self.space_min_len, self.space_max_len))

        return "".join(string)


class StringImageComposer(object):
    def __init__(
        self, nist_root, stroke_range=(0, 0), accessor=RandomGet, normalize=False
    ):

        self.nist_accessor = accessor(nist_root)
        self.color_inverse = True
        self.normalize = normalize

        if stroke_range[0] > stroke_range[1]:
            stroke_range = stroke_range[::-1]
        self.stroke_range = stroke_range

    def __call__(self, string):

        if " " in [string[0], string[-1]]:
            raise ValueError('Blank (" ") cannot be the first or the last of string')

        img_seq = [
            self.nist_accessor(char, strip_bg=True, color_inverse=self.color_inverse)
            if char != " "
            else None
            for char in string
        ]

        if self.normalize:

            def _normalize(img):
                img_min = np.min(img)
                img_max = np.max(img)
                return ((img - img_min) * 255.0 / (img_max - img_min)).astype(img.dtype)

            img_seq = [_normalize(img) if img is not None else None for img in img_seq]

        syn_img = img_seq[0]
        for idx in range(1, len(img_seq)):

            next_char = img_seq[idx]
            if next_char is None:
                next_char = np.zeros(
                    (syn_img.shape[0], syn_img.shape[0]), dtype=syn_img.dtype
                )

            cur_height = syn_img.shape[0]
            cur_width = syn_img.shape[1]
            next_height = next_char.shape[0]
            next_width = next_char.shape[1]

            pad_height_total = int(max(next_height - cur_height, 0))
            pad_height = int(pad_height_total / 2)
            syn_img = np.pad(
                syn_img,
                (
                    (pad_height, pad_height_total - pad_height),
                    (0, next_width + randrange(*self.stroke_range)),
                ),
            )

            y_pos = int((syn_img.shape[0] - next_height) / 2)
            syn_img[y_pos : y_pos + next_height, -next_width:, ...] = next_char

        return syn_img


class BackgroundComposer(object):
    def __init__(
        self,
        color_range,
        noise_scale,
        border_ratio,
        luminance_range,
        erode_size=None,
        max_blur_size=None,
    ):

        self.color_range = color_range
        self.noise_scale = noise_scale
        self.border_ratio = border_ratio
        self.luminance_range = luminance_range
        self.erode_size = erode_size
        self.max_blur_size = max_blur_size

    def __call__(self, image):

        if self.erode_size is not None:
            erode_size = self.erode_size
            pad_size = int(erode_size / 2)
            elem = cv.getStructuringElement(
                cv.MORPH_ELLIPSE, (erode_size, erode_size), (pad_size, pad_size)
            )
            image = cv.erode(np.pad(image, pad_size), elem)

        if self.max_blur_size is not None:
            blur_size = randint(0, self.max_blur_size)
            if blur_size > 0:
                image = cv.blur(image, (blur_size, blur_size))

        min_lum = self.luminance_range[0]
        max_lum = self.luminance_range[1]
        lum = random() * (max_lum - min_lum) + min_lum
        image = (image * lum).astype(np.uint8)

        height, width = image.shape

        canvas_height = int(height * self.border_ratio)
        canvas_width = int(width * self.border_ratio)
        if self.color_range is not None:
            canvas = np.full(
                (canvas_height, canvas_width),
                randint(*self.color_range),
                dtype=np.uint8,
            )
        else:
            color = int(np.mean(image[image > 0]))
            canvas = np.full((canvas_height, canvas_width), color, dtype=np.uint8)

        if self.noise_scale is not None:
            noise = np.random.normal(scale=self.noise_scale, size=canvas.shape)
            canvas = np.clip(canvas + noise, 0, 255).astype(np.uint8)

        y_start = int((canvas_height - height) / 2)
        y_end = y_start + height
        x_start = int((canvas_width - width) / 2)
        x_end = x_start + width

        canvas[y_start:y_end, x_start:x_end] = np.maximum(
            canvas[y_start:y_end, x_start:x_end], image
        )

        return canvas
