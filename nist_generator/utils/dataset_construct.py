import numpy as np
import cv2 as cv

from os import listdir
from os.path import expanduser, join, isdir, basename

from glob import glob
from tqdm import tqdm
from random import randrange


class CharSet(object):
    def __init__(self, char_dir):

        # List all support characters
        char_dir = expanduser(char_dir)
        dataset = {}
        for child in tqdm(listdir(char_dir), desc="Gathering digit samples"):
            key = basename(child)
            tmp_path = join(char_dir, child)
            if isdir(tmp_path):
                dataset[key] = list(glob(join(tmp_path, "*.jpg"))) + list(
                    glob(join(tmp_path, "*.png"))
                )

        self.dataset = dataset

    def __call__(self, key, idx=None, strip_bg=False, color_inverse=False):

        if key not in self.dataset:
            raise ValueError(f'"{key}" is not support by NIST dataset')
        if idx is None:
            idx = randrange(0, len(self.dataset[key]))

        return cv.imread(self.dataset[key][idx], cv.IMREAD_GRAYSCALE)

    def supported_char(self):
        return list(self.dataset.keys())


class DatasetConstructor(object):
    def __init__(self, nist_root):

        nist_root = expanduser(nist_root)

        # List all supported characters
        dataset = {}
        for child in tqdm(listdir(nist_root), desc="Gathering samples"):
            tmp_path = join(nist_root, child)
            if isdir(tmp_path):
                try:
                    key = chr(int("0x%s" % child, base=16))
                except:
                    msg = (
                        f'Cannot decode subdirectory name "{child}" as ASCII character. '
                        + "Do you use wrong dataset path for NIST?"
                    )
                    raise RuntimeError(msg)

                dataset[key] = list(glob(join(tmp_path, "*/*.png")))

        self.dataset = dataset

    def __call__(self, key, idx=None, strip_bg=False, color_inverse=False):

        if key not in self.dataset:
            raise ValueError(f'"{key}" is not support by NIST dataset')
        if idx is None:
            idx = randrange(0, len(self.dataset[key]))

        img = cv.imread(self.dataset[key][idx], cv.IMREAD_GRAYSCALE)
        inverse_img = (255 - img).astype(np.uint8)

        if color_inverse:
            ret_img = inverse_img
        else:
            ret_img = img

        if strip_bg:
            (x, y, w, h) = cv.boundingRect(inverse_img)
            ret_img = ret_img[y : y + h, x : x + w, ...]

        return ret_img.copy()


class RandomGet(DatasetConstructor):
    def __init__(self, nist_root):
        super().__init__(nist_root)
