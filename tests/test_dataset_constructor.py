import cv2 as cv

from nist_generator.utils.dataset_construct import DatasetConstructor
from nist_generator.utils import RandomGet


if __name__ == '__main__':

    # Test dataset constructor
    data = DatasetConstructor('~/dataset/nist/by_class')

    img = data('a', 0)
    cv.imshow('digit', img)
    cv.waitKey(0)

    img = data('a', 0, color_inverse=True)
    cv.imshow('digit', img)
    cv.waitKey(0)

    img = data('a', 0, strip_bg=True)
    cv.imshow('digit', img)
    cv.waitKey(0)

    img = data('a', 0, strip_bg=True, color_inverse=True)
    cv.imshow('digit', img)
    cv.waitKey(0)

    # Test random get
    data = RandomGet('~/dataset/nist/by_class')

    img = data('a')
    cv.imshow('digit', img)
    cv.waitKey(0)

    img = data('a', color_inverse=True)
    cv.imshow('digit', img)
    cv.waitKey(0)

    img = data('a', strip_bg=True)
    cv.imshow('digit', img)
    cv.waitKey(0)

    img = data('a', strip_bg=True, color_inverse=True)
    cv.imshow('digit', img)
    cv.waitKey(0)
