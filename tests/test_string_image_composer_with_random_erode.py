import cv2 as cv

from nist_generator.utils import (
    RandomStringGenerator,
    StringImageComposer,
    BackgroundComposer,
    RandomErode,
)


if __name__ == "__main__":

    text_generator = RandomStringGenerator(
        char_set="/home/james/Desktop/Tomonejam/Dataset/ppocr_word_dict.txt",
        # 字元集，可以是使用換行符號切開的檔案
        # https://github.com/PaddlePaddle/PaddleOCR/blob/release/2.3/doc/doc_en/recognition_en.md#13-dictionary
        # 也可以帶入 list: ['A', 'B', 'C', ...]
        #
        char_min_len=5,
        # 最短字串產生長度（不包含空白字元）
        #
        char_max_len=10,
        # 最長字串產生長度（不包含空白字元）
        #
        space_max_strike=2,
        # 最大空白出現的次數
        #
        space_min_len=1,
        # 每個空白的最短長度
        #
        space_max_len=3
        # 每個空白的最長長度
        #
    )

    rand_eroder = RandomErode(
        ratio=0.005,
        # 破壞點佔所有前景 Pixel 的比例，前景判定的方式為 (Pixel 值 > fg_thresh)
        #
        fg_thresh=128,
        # 判定前景、背景的門檻值
        #
        kernel_size=15,
        # 針對破壞點進行擴張的 kernel size，將破壞點變成破壞面
        #
        fg_erode_kernel_size=3
        # 書將輸入影像進行侵蝕之後再進行前景、背景判斷，此參數為侵蝕的 kernel size，
        # 有助於避免破壞點中心太靠近筆劃邊緣，導致破壞效果不佳
        #
    )

    img_composer = StringImageComposer(
        "~/dataset/nist/by_class",
        # nist 資料夾路徑，請到：https://www.nist.gov/srd/nist-special-database-19
        # 下載 by_class.zip 並解壓縮，再將路徑帶入這個參數
        #
        stroke_range=(2, 7)
        # 字元間格產生區間（Pixel）
        #
    )

    bg_composer = BackgroundComposer(
        color_range=(32, 48),
        # 背景填充色的數值區間（灰階）
        #
        noise_scale=10,
        # 背景雜訊強度
        #
        border_ratio=1.2,
        # 幫字串圖加上一些邊框，新的 width 與 height 為輸入圖大小 * border_ratio
        #
        erode_size=3,
        # 侵蝕輸入字串（nist 的手寫都很粗，比賽圖的手寫十分細）
        #
        max_blur_size=5,
        # 對輸入圖片進行隨機模糊處理，此參數為最大模糊 kernel 的大小
        #
        luminance_range=(0.5, 1.00)
        # 對輸入圖片進行隨機亮度調整
        #
    )

    # 產生範例
    for i in range(50):

        text = text_generator()
        img = img_composer(text)
        img = rand_eroder(img)
        img = bg_composer(img)

        cv.imshow("result", img)
        if cv.waitKey(0) == 27:
            break
