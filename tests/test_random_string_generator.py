from nist_generator.utils.generator import RandomStringGenerator


if __name__ == '__main__':

    str_generator = RandomStringGenerator(
        char_set='/home/james/Desktop/Tomonejam/Dataset/ppocr_word_dict.txt',
        char_min_len=5,
        char_max_len=10,
        space_max_strike=2,
        space_min_len=1,
        space_max_len=3
    )

    for i in range(10):
        print(str_generator())
