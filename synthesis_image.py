import glob
import cv2 as cv
from pathlib import Path
import numpy as np
from scipy.ndimage.morphology import grey_closing, grey_opening
import random

def prepare_backgorund(image, contours):
    mask = np.ones_like(image)
    mask = cv.fillPoly(mask, pts=contours, color=(0,0,0))
    #mask = mask[:,:,0]
    bg = image.copy()
    bg = cv.bitwise_and(bg, bg, mask=mask)
    return bg

def cut_text_from_src(srcimage, rect):
    width = 800
    height = 100
    src =  np.array([
        [0, 0],
        [width - 1, 0],
        [width - 1, height - 1],
        [0, height - 1]], dtype = "float32")
    M = cv.getPerspectiveTransform(rect, src)
    cut = cv.warpPerspective(srcimage, M, (width, height))
    return cut

def cut_text_from_p(p):
    srcimage = cv.imread(p, cv.IMREAD_GRAYSCALE)
    basename = Path(p).name
    gt = "/notebooks/MaskTextSpotterV3_original/datasets/icdar2015/train_gts/{}.txt".format(basename)
    with open(gt, 'r') as f:
        l = f.readlines()[0]
        rect = np.array(l.split(',')[0:8]).reshape((-1, 2)).astype("float32")
        text = l.split(',')[8]
    cut = cut_text_from_src(srcimage, rect)
    return cut, text
    
def random_select_image():
    paths = glob.glob("/notebooks/MaskTextSpotterV3_original/datasets/icdar2015/train_images/*.jpg")
    p = random.choice(paths)
    return p

def paste_cut_to_bg(bgimage, rect, cut, flip=0):
    width = cut.shape[1]
    height = cut.shape[0]
    src =  np.array([
        [0, 0],
        [width - 1, 0],
        [width - 1, height - 1],
        [0, height - 1]], dtype = "float32")
    rect2 = np.copy(rect)
    if flip:
        rect2[[0, 2]] = rect2[[2, 0]]
        rect2[[1, 3]] = rect2[[3, 1]]
    M = cv.getPerspectiveTransform(src, rect2)
    warped = cv.warpPerspective(cut, M, (bgimage.shape[1], bgimage.shape[0]))
    composed = bgimage + warped
    return composed

def do_close(img): #black bigger
    mat = np.ones((10,10))
    processed = grey_closing(img, structure=mat)
    return processed

def do_open(img): #white bigger
    mat = np.ones((10,10))
    processed = grey_opening(img, structure=mat)
    return processed

def produce_one_synthesis_sample(cut_image, gt_text, bgimage_p, flip=0, distortion=None):
    srcimage = cv.imread(bgimage_p, cv.IMREAD_GRAYSCALE)
    basename = Path(bgimage_p).name
    gt = "/notebooks/MaskTextSpotterV3_original/datasets/icdar2015/train_gts/{}.txt".format(basename)
    with open(gt, 'r') as f:
        l = f.readlines()[0]
        rect = np.array(l.split(',')[0:8]).reshape((-1, 2)).astype("float32")
    bgimage = prepare_backgorund(srcimage, [rect.astype(int)])
    cut_processed = cut_image.copy()
    if distortion:
        for d in distortion:
            cut_processed = d(cut_processed)
    composed = paste_cut_to_bg(bgimage, rect, cut_processed, flip)
    gt_list = l.split(',')[0:8]
    gt_list.append(gt_text)
    gt_str = ",".join(gt_list)
    return composed, gt_str